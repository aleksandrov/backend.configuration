﻿using Autofac;
using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration.Autofac
{
    public class ConfigurationModule<TConfiguration> : Module where TConfiguration: IConfiguration
    {
        private readonly IConfigurationService<TConfiguration> service;

        public ConfigurationModule(IConfigurationService<TConfiguration> service)
        {
            this.service = service;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(ctx => service).As<IConfigurationService<TConfiguration>>().ExternallyOwned();
            builder.Register(ctx => ctx.Resolve<IConfigurationService<TConfiguration>>().Configuration).As<TConfiguration>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
