﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration.Parameters
{
    class UndefinedParameter : ConfigurationParameter
    {
        public UndefinedParameter(string name, IConfigurationSource source) : base(name, source, null)
        {
        }
    }
}
