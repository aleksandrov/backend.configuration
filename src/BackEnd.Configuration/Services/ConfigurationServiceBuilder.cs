﻿using System;
using System.Collections.Generic;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Sources;

namespace BackEnd.Configuration.Services
{
	public class ConfigurationServiceBuilder<T> where T : IConfiguration
	{
		IConfigurationFactory<T> factory;
	    readonly IList<IConfigurationSource> overrides;
		bool defaultOrder;
	    readonly DefaultsSource defaultValues;

	    readonly ConnectionStringsSource connections;
	    readonly AppConfigSource appConfig;
	    readonly EnvVariableSource environment;
	    readonly CommandLineSource commandLine;

		public ConfigurationServiceBuilder(params string[] args)
		{
			overrides = new List<IConfigurationSource>();
			defaultValues = new DefaultsSource();

			connections = new ConnectionStringsSource();
			appConfig = new AppConfigSource();
			environment = new EnvVariableSource();
			commandLine = new CommandLineSource(args);

			OverrideByConnectionStrings();
			OverrideByAppSettings();
			OverrideByEnvVariables();
			OverrideByCommandLine();

			defaultOrder = true;
		}

		private void ResetDefaultOrder()
		{
			if (defaultOrder)
			{
				defaultOrder = false;
				overrides.Clear();
			}
		}

		public ConfigurationServiceBuilder<T> OverrideByConnectionStrings()
		{
			ResetDefaultOrder();
			overrides.Add(connections);
			return this;
		}

		public ConfigurationServiceBuilder<T> OverrideByAppSettings()
		{
			ResetDefaultOrder();
			overrides.Add(appConfig);
			return this;	
		}

		public ConfigurationServiceBuilder<T> OverrideByEnvVariables()
		{
			ResetDefaultOrder();
			overrides.Add(environment);
			return this;	
		}

		public ConfigurationServiceBuilder<T> OverrideByCommandLine()
		{
			ResetDefaultOrder();
			overrides.Add(commandLine);
			return this;	
		}

		public ConfigurationServiceBuilder<T> WithEnvironment(Action<EnvVariableSource> configure)
		{
			if (configure == null) throw new ArgumentNullException("configure");

			configure(environment);
			return this;	
		}

		public ConfigurationServiceBuilder<T> UsingFactory(IConfigurationFactory<T> factory)
		{
			this.factory = factory;
			return this;
		}

		public ConfigurationServiceBuilder<T> WithDefault(string key, string value)
		{
			defaultValues.AddDefaultValue(key, value);
			return this;	
		}

		public IConfigurationService<T> Build()
		{
			var composite = new CompositeSource(defaultValues);

			foreach (var source in overrides)
			{
				composite.OverrideBy(source);
			}

			return new ConfigurationService<T>(composite, factory);
		}
	}
}

