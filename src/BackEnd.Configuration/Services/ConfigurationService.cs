﻿using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration.Services
{
    public class ConfigurationService<T> : IConfigurationService<T> where T : IConfiguration
    {
        private readonly IConfigurationFactory<T> factory;
        private readonly IConfigurationSource source;

        private T configuration;

		public static ConfigurationServiceBuilder<T> Init(params string[] args) {
			return new ConfigurationServiceBuilder<T>(args);
		}

		internal ConfigurationService(IConfigurationSource source, IConfigurationFactory<T> factory)
		{
			this.factory = factory;
			this.source = source;
		}

        public T Configuration
        {
            get
            {
                if (configuration == null)
                {
                    configuration = factory.CreateConfiguration(source);
                }
                return configuration;
            }
        }
    }
}
