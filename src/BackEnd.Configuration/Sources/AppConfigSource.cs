﻿using System.Configuration;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;

namespace BackEnd.Configuration.Sources
{
	class AppConfigSource : SourceBase
    {
        public override ConfigurationParameter Get(string key)
        {
            var value = ConfigurationManager.AppSettings[key];
            if (value == null) return new UndefinedParameter(key, this);

            return new ConfigurationParameter(key, this, ConfigurationManager.AppSettings[key]);
        }

        public override string Name
        {
            get { return "Application Configuration"; }
        }


    }
}
