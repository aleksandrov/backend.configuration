﻿using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration.Sources
{
	public abstract class SourceBase : IConfigurationSource
	{
		protected SourceBase()
		{
		}

		public abstract ConfigurationParameter Get(string key);

		public virtual string Name 
		{ 
			get {
				return GetType().Name;
			} 
		}

		public virtual string GetParameterInfo(string key)
		{
			var parameter = Get(key);
			return string.Format("{0} = {1} (from {2})", parameter.Name, parameter.Value, parameter.Source.Name);
		}
	}
}

