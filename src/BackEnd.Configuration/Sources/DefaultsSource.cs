﻿using System;
using System.Collections.Generic;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;

namespace BackEnd.Configuration.Sources
{
	class DefaultsSource : SourceBase
    {
        private readonly IDictionary<string, ConfigurationParameter> defaults; 

        public DefaultsSource()
        {
            defaults = new Dictionary<string, ConfigurationParameter>();    
        }

        public void AddDefaultValue(string key, string value)
        {
            if (key == null) throw new ArgumentNullException("key");
            if (value == null) throw new ArgumentNullException("value");

            defaults.Add(key, new ConfigurationParameter(key, this, value));
        }

		public override ConfigurationParameter Get(string key)
        {
            if (defaults.ContainsKey(key))
            {
                return defaults[key];
            }

            return new UndefinedParameter(key, this);
        }

        public override string Name
        {
            get { return "Defaults"; }
        }
    }
}
