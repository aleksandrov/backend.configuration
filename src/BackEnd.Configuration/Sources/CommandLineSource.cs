﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;

namespace BackEnd.Configuration.Sources
{
	class CommandLineSource : SourceBase
    {
        private readonly IDictionary<string, ConfigurationParameter> parameters; 

        public CommandLineSource(params string[] args)
        {
            var parsed = args.Select(arg => arg.Split('=')).Where(x => x.Length == 2).Select(x => new {key = x[0], value = x[1]});

            parameters = new Dictionary<string, ConfigurationParameter>();
            foreach (var arg in parsed)
            {
                parameters.Add(arg.key, new ConfigurationParameter(arg.key, this, arg.value));
            }
        }

		public override ConfigurationParameter Get(string key)
        {
            if (key == null) throw new ArgumentNullException("key");

            if (parameters.ContainsKey(key))
            {
                return parameters[key];
            }

            return new UndefinedParameter(key, this);
        }

        public override string Name
        {
            get { return "Command Line"; }
        }

    }
}
