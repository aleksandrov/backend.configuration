﻿using System;
using System.Collections.Generic;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;

namespace BackEnd.Configuration.Sources
{
	public class EnvVariableSource : SourceBase
    {
	    readonly IDictionary<string, string> mappings;

		public EnvVariableSource()
		{
			mappings = new Dictionary<string, string>();
		}

		public override ConfigurationParameter Get(string key)
        {
			if (key == null) throw new ArgumentNullException("key");

			string workingKey = key;

			if (mappings.ContainsKey(key))
			{
				workingKey = mappings[key];
			}

			workingKey = workingKey.Replace('.', '_');

			var value = System.Environment.GetEnvironmentVariable(workingKey);
			if (value == null)
			{
				workingKey = workingKey.ToUpperInvariant();
				value = System.Environment.GetEnvironmentVariable(workingKey);
			}

            if (value == null) return new UndefinedParameter(key, this);

            return new ConfigurationParameter(key, this, value);
        }

        public override string Name
        {
			get { return string.Format("Environment Variables"); }
        }

		public void MapVariable(string variable, string key)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (variable == null) throw new ArgumentNullException("variable");


			if (mappings.ContainsKey(key))
			{
				mappings[key] = variable;
			}
			else {
				mappings.Add(key, variable);	
			}

		}

		public override string GetParameterInfo(string key)
		{
			var result = base.GetParameterInfo(key);

			if (mappings.ContainsKey(key))
			{
				result = string.Format("{0} [{1} => {2}]", result, mappings[key], key);
			}

			return result;
		}
    }
}
