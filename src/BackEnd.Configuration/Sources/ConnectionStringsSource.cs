﻿using System.Configuration;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;


namespace BackEnd.Configuration.Sources
{
	class ConnectionStringsSource : SourceBase
    {
        public override ConfigurationParameter Get(string key)
        {
            var value = ConfigurationManager.ConnectionStrings[key];
            if (value == null) return new UndefinedParameter(key, this);

            return new ConfigurationParameter(key, this, value.ConnectionString);
        }

        public override string Name
        {
            get { return "Connection Strings"; }
        }
    }
}
