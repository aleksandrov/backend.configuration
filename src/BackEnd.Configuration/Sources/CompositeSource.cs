﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Parameters;

namespace BackEnd.Configuration.Sources
{
	class CompositeSource : SourceBase
    {
        private readonly IList<IConfigurationSource> sources;

        public CompositeSource(IConfigurationSource baseSource)
        {
            if (baseSource == null) throw new ArgumentNullException("baseSource");

            sources = new List<IConfigurationSource>();
            sources.Add(baseSource);
        }

        public CompositeSource OverrideBy(IConfigurationSource source)
        {
            if (source == null) throw new ArgumentNullException("source");
            sources.Add(source);

            return this;
        }

		public override ConfigurationParameter Get(string key)
        {
            var orderedSources = sources.Reverse();
            ConfigurationParameter value = new UndefinedParameter(key, this);
            foreach (var source in orderedSources)
            {
                value = source.Get(key);
                if (!(value is UndefinedParameter)) return value;
            }

            return value;
        }

        public override string Name
        {
            get { return "Composite"; }
        }

    }
}
