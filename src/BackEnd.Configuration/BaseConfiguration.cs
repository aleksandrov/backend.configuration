﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration
{
    public class BaseConfiguration : IConfiguration
    {
        private readonly IDictionary<string, ConfigurationParameter> parameters;

        protected BaseConfiguration()
        {
            parameters = new Dictionary<string, ConfigurationParameter>();
        }

        protected void AddParameter(ConfigurationParameter parameter)
        {
            parameters.Add(parameter.Name, parameter);
        }

        public IEnumerable<ConfigurationParameter> GetParameters()
        {
            return parameters.Values;
        }

        public ConfigurationParameter this[string key]
        {
            get
            {
                if (parameters.ContainsKey(key))
                {
                    return parameters[key];
                }

                return null;
            }
        }

    }
}
