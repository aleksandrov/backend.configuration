﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;

namespace BackEnd.Configuration
{
    public static class ConfigurationExtensions
    {
        public static void Dump(this IConfiguration configuration, TextWriter writer)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");
            if (writer == null) throw new ArgumentNullException("writer");

            var parameters = configuration.GetParameters().OrderBy(p => p.Name).ToList();
            foreach (var parameter in parameters)
            {
                writer.WriteLine(parameter.Source.GetParameterInfo(parameter.Name));
            }
        }

        public static string DumpToString(this IConfiguration configuration)
        {
            using (var writer = new StringWriter())
            {
                configuration.Dump(writer);
                return writer.ToString();
            }
        }
    }
}
