﻿using System.Collections.Generic;

namespace BackEnd.Configuration.Interfaces
{
    public interface IConfiguration
    {
        IEnumerable<ConfigurationParameter> GetParameters();
    }
}
