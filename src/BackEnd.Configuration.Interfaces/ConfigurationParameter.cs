﻿namespace BackEnd.Configuration.Interfaces
{
    public class ConfigurationParameter
    {
        public ConfigurationParameter(string name, IConfigurationSource source, string value = null)
        {
            Name = name;
            Source = source;
            Value = value;
        }

        public string Name { get; private set; }

        public string Value { get; private set; }

        public IConfigurationSource Source { get; private set; }
    }
}
