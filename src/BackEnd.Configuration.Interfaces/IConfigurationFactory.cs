﻿namespace BackEnd.Configuration.Interfaces
{
    public interface IConfigurationFactory<TConfiguration> where TConfiguration : IConfiguration
    {
        TConfiguration CreateConfiguration(IConfigurationSource source);
    }
}
