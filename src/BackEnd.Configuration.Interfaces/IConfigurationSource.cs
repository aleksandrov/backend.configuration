﻿namespace BackEnd.Configuration.Interfaces
{
    public interface IConfigurationSource
    {
        ConfigurationParameter Get(string key);

        string Name { get; }

		string GetParameterInfo(string key);
    }
}
