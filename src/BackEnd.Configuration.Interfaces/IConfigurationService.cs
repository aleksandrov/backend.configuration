﻿namespace BackEnd.Configuration.Interfaces
{
    public interface IConfigurationService<TConfiguration> where TConfiguration: IConfiguration
    {
        TConfiguration Configuration { get; }
    }
}
