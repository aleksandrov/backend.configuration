﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;
using BackEnd.Configuration.Services;
using Machine.Fakes;
using Machine.Fakes.ReSharperAnnotations;
using Machine.Specifications;
using NSubstitute;

namespace BackEnd.Configuration.Specs.Services
{
    public interface ITestConfiguration : IConfiguration
    {
        string Value { get; }
    }

    [Subject(typeof(ConfigurationService<>))]
    class when_getting_Test_Configuration_for_the_first_time : ConfigurationServiceSpecs
    {
        Establish context = () => {  };

        private Because of = () => conf = Subject.Configuration;

        private It should_call_Factory_to_create_Configuration = () => The<IConfigurationFactory<ITestConfiguration>>().Received().CreateConfiguration(The<IConfigurationSource>());

        private static ITestConfiguration conf;
    }

    [Subject(typeof(ConfigurationService<>))]
    class when_getting_Test_Configuration_next_time : ConfigurationServiceSpecs
    {
        Establish context = () =>
        {
            conf = Subject.Configuration;

            The<IConfigurationFactory<ITestConfiguration>>().ClearReceivedCalls();
        };

        private Because of = () => conf = Subject.Configuration;

        private It should_not_call_Factory = () => The<IConfigurationFactory<ITestConfiguration>>().DidNotReceive().CreateConfiguration(The<IConfigurationSource>());

        private static ITestConfiguration conf;
    }

    class ConfigurationServiceSpecs : WithSubject<ConfigurationService<ITestConfiguration>>
    {
        Establish context = () =>
        {
            Subject = new ConfigurationService<ITestConfiguration>(The<IConfigurationSource>(), The<IConfigurationFactory<ITestConfiguration>>());
        }; 
    }
}
