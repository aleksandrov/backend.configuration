﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Sources;
using NUnit.Framework;

namespace BackEnd.Configuration.Specs.Sources
{
    [TestFixture]
    class CommandLineSourceTests
    {
        [Test]
        public void should_not_throw_when_unparsible_arg_found()
        {
            CommandLineSource source = new CommandLineSource("--dry-run");
        }
    }
}
