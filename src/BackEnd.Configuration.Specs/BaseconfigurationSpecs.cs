﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd.Configuration.Interfaces;
using Machine.Fakes;
using Machine.Specifications;

namespace BackEnd.Configuration.Specs
{

    [Subject(typeof(BaseConfiguration))]
    class when_adding_new_parameter : WithSubject<TestConfiguration>
    {
        Establish context = () =>
        {
            p = new ConfigurationParameter("name", The<IConfigurationSource>(), "value");
        };

        private Because of = () => Subject.Add(p);

        private It should_return_added_parameter = () => { };

        private static ConfigurationParameter p;
    }

    class TestConfiguration : BaseConfiguration
    {
        public void Add(ConfigurationParameter parameter)
        {
            AddParameter(parameter);
        }
    }
}
