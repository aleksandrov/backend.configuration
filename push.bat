set VERSION=1.0.0.15

cd output\BackEnd.Configuration
..\..\.nuget\NuGet.exe push BackEnd.Configuration.%VERSION%.nupkg
rem ..\..\.nuget\NuGet.exe delete BackEnd.Configuration %VERSION% -NoPrompt

cd ..\..\
cd output\BackEnd.Configuration.Autofac
..\..\.nuget\NuGet.exe push BackEnd.Configuration.Autofac.%VERSION%.nupkg
rem ..\..\.nuget\NuGet.exe delete BackEnd.Configuration.Autofac %VERSION% -NoPrompt
