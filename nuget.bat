set VERSION=1.0.0.15

cd output\BackEnd.Configuration
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\BackEnd.Configuration.Autofac
..\..\.nuget\NuGet.exe pack -Version %VERSION%
