cd src

msbuild /t:Rebuild /p:Configuration=Release /m

cd ..

rmdir output /s /q
mkdir output
mkdir output\lib

copy src\BackEnd.Configuration\bin\Release\BackEnd.Configuration*.* output\lib
copy src\BackEnd.Configuration.Autofac\bin\Release\BackEnd.Configuration.Autofac*.* output\lib

mkdir output\BackEnd.Configuration
mkdir output\BackEnd.Configuration\lib
mkdir output\BackEnd.Configuration\lib\net40
copy src\BackEnd.Configuration\bin\Release\BackEnd.Configuration.dll output\BackEnd.Configuration\lib\net40
copy src\BackEnd.Configuration\bin\Release\BackEnd.Configuration.Interfaces.dll output\BackEnd.Configuration\lib\net40
copy src\BackEnd.Configuration\bin\Release\BackEnd.Configuration.nuspec output\BackEnd.Configuration

mkdir output\BackEnd.Configuration.Autofac
mkdir output\BackEnd.Configuration.Autofac\lib
mkdir output\BackEnd.Configuration.Autofac\lib\net40
copy src\BackEnd.Configuration.Autofac\bin\Release\BackEnd.Configuration.Autofac.dll output\BackEnd.Configuration.Autofac\lib\net40
copy src\BackEnd.Configuration.Autofac\bin\Release\BackEnd.Configuration.Autofac.nuspec output\BackEnd.Configuration.Autofac
