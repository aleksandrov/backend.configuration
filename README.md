Main goal of the library is to get an abstraction layer over different sources of application configuration parameters. Library currently supports following sources (First source has the highest priority):

1. command-line options
1. environment variables
1. app settings
1. connection strings
1. defaults

Library tries to get parameter from the source with the highest priority. If no value found in that source, library tries to get value from source with lower priority.

# Usage Example #

``` csharp
var configurationService = ConfigurationService<IMyConfiguration>
    .Init(args)
    .UsingFactory(new MyConfigurationFactory())
    .WithEnvironment(env =>
    {
         env.MapVariable("TEST", MyConfigurationFactory.TEST);
         env.MapVariable("DATABASE", MyConfigurationFactory.DATABASE);
         env.MapVariable("URL", MyConfigurationFactory.URL);
    })
    .Build();

```